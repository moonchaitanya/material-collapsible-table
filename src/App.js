import React from 'react';
import logo from './logo.svg';
import './App.css';
import Inceptiontable from './component/materialTable'

function App() {
  return (
    <div className="App" style= {{ marginTop: "75px"}}>
      <h2>POC Collapsible Table</h2>
     <Inceptiontable></Inceptiontable>
    </div>
  );
}

export default App;
