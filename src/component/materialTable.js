import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import DeleteIcon from '@material-ui/icons/Delete';

const useRowStyles = makeStyles({
  root: {
    '& > *': {
      borderBottom: 'unset',
    },
  },
});

function createData(name, calories, fat, carbs, protein, price) {
  return {
    name,
    calories,
    fat,
    carbs,
    protein,
    price,
    history: [
      { date: '2020-01-05', customerId: '11091700', amount: 3 },
      { date: '2020-01-02', customerId: 'Anonymous', amount: 1 },
    ],
  };
}

const rows = [
    {
        id: 1,
        name: 'Shakira Biddy',
        cost: '201: Enterprises Sales',
        current: 6,
        increment: 13,
        budget: 100,
        remaining: 50,
        estimate: 125,
        status: "review",
        history: [
            { date: '2020-01-05', customerId: '11091700', amount: 3 },
            { date: '2020-01-02', customerId: 'Anonymous', amount: 1 },
          ]
    },
    {
        id: 2,
        name: 'Erasmo Ackreson',
        cost: '202: Enterprises Sales',
        current: 6,
        increment: 15,
        budget: 1000,
        remaining: 500,
        estimate: 95,
        status: "review",
    },
    {
        id: 3,
        name: 'Simon Toffel',
        cost: '203: Enterprises Sales',
        current: 7,
        increment: 15,
        budget: 7000,
        remaining: 250,
        estimate: 85,
        status: "review",
    },
    {
        id: 4,
        name: 'Rohit patil',
        cost: '204: Enterprises Sales',
        current: 10,
        increment: 18,
        budget: 7000,
        remaining: 900,
        estimate: 65,
        status: "review"
    },
    {
        id: 5,
        name: 'Salman Shiekh',
        cost: '205: Enterprises Sales',
        current: 5,
        increment: 11,
        budget: 1100,
        remaining: 60,
        estimate: 75,
        status: "review"
    }
]

const second = [
    {
        id:1,
        title: 'Sales Manager',
        desc: 'Enterprise Sales Growth',
        type: 'Full Time',
        hours: 40,
        location: 'Nagpur',
        level: 'Staff',
        function: 'Sales',
        month: 'May',
        cost: '$50000',
        bow: '$567777',
        status: 'Hired',
        job: 'Hired'
    },
    {
        id:2,
        title: 'General Manager',
        desc: 'Enterprise Sales Growth',
        type: 'Full Time',
        hours: 40,
        location: 'Pune',
        level: 'Staff',
        function: 'Manager',
        month: 'April',
        cost: '$800000',
        bow: '$657777',
        status: 'Hired',
        job: 'Hired'
    },
    {
        id:3,
        title: 'Customer Manager',
        desc: 'Enterprise Sales Growth',
        type: 'Full Time',
        hours: 30,
        location: 'Mumbai',
        level: 'Staff',
        function: 'Sales',
        month: 'June',
        cost: '$60000',
        bow: '$897777',
        status: 'Hired',
        job: 'Hired'
    },
    {
        id:4,
        title: 'Sales Manager',
        desc: 'Enterprise Sales Growth',
        type: 'Full Time',
        hours: 40,
        location: 'Delhi',
        level: 'Staff',
        function: 'Sales',
        month: 'May',
        cost: '$90000',
        bow: '$557777',
        status: 'Hired',
        job: 'Hired'
    }
]

const third = [
  {
      id:1,
      title: 'Sales Manager',
      desc: 'Enterprise Sales Growth',
      type: 'Full Time',
      hours: 40,
      location: 'Nagpur',
      level: 'Staff',
      status: 'Hired'
  },
  {
      id:2,
      title: 'General Manager',
      desc: 'Enterprise Sales Growth',
      type: 'Full Time',
      hours: 40,
      location: 'Pune',
      level: 'Staff',
      status: 'Hired'
  },
  {
    id:3,
    title: 'General Manager',
    desc: 'Enterprise Sales Growth',
    type: 'Full Time',
    hours: 40,
    location: 'Pune',
    level: 'Staff',
    status: 'Hired'
  },
  {
    id:4,
    title: 'General Manager',
    desc: 'Enterprise Sales Growth',
    type: 'Full Time',
    hours: 40,
    location: 'Pune',
    level: 'Staff',
    status: 'Hired'
  }
]

function Mainrow(props) {
  const { row } = props;
  const [open, setOpen] = React.useState(false);
  const [openSecondTable, setOpenSecondTable] = React.useState(false);
  const classes = useRowStyles();

  return (
    <React.Fragment>
      <TableRow className={classes.root}>
        <TableCell>
          <IconButton aria-label="expand row" size="small" onClick={() => setOpen(!open)}>
            {open ? <KeyboardArrowDownIcon /> : <KeyboardArrowRight />}
          </IconButton>
        </TableCell>
        <TableCell align="center">{row.id}</TableCell>
        <TableCell align="center">{row.name}</TableCell>
        <TableCell align="center">{row.cost}</TableCell>
        <TableCell align="center">{row.current}</TableCell>
        <TableCell align="center">{row.increment}</TableCell>
        <TableCell align="center">{row.budget}</TableCell>
        <TableCell align="center">{row.remaining}</TableCell>
        <TableCell align="center">{row.estimate}</TableCell>
        <TableCell align="center">{row.status}</TableCell>
      </TableRow>
      <TableRow>
        <TableCell style={{ paddingBottom: 0, paddingTop: 0, paddingLeft: 0, paddingRight: 0 }} colSpan={10}>
          <Collapse in={open} timeout="auto" unmountOnExit>
            
              <Typography component="h5" style={{ fontWeight: 'bold', textDecoration: 'underline',  marginLeft: "50px" }}>
                Increment Direct Report Asks
              </Typography>
              <TableContainer component={Paper}>
                <Table aria-label="collapsible table">
                    <TableHead>
                    <TableRow>
                        <TableCell style={{'width': '5%'}}/>
                        <TableCell align="center">Sr. No</TableCell>
                        <TableCell align="center">Job Title</TableCell>
                        <TableCell align="center">Description</TableCell>
                        <TableCell align="center">Job Type</TableCell>
                        <TableCell align="center">Hours</TableCell>
                        <TableCell align="center">Location</TableCell>
                        <TableCell align="center">Level</TableCell>
                        <TableCell align="center">Function</TableCell>
                        <TableCell align="center">Start Month</TableCell>
                        <TableCell align="center">Est. Cost</TableCell>
                        <TableCell align="center">Est. Bow Wave</TableCell>
                        <TableCell align="center">Action</TableCell>
                        <TableCell align="center">Status</TableCell>
                        <TableCell align="center">ATS Job Req</TableCell>
                    </TableRow>
                    </TableHead>
                    <TableBody>
                     {second.map((row) => (
                      <Firstrow key={row.id} row={row} />
                     ))}
                    </TableBody>
                </Table>
             </TableContainer>
            
          </Collapse>
        </TableCell>
      </TableRow>
    </React.Fragment>
  );
}

function Firstrow(props) {
  const { row } = props;
  const [openSecondTable, setOpenSecondTable] = React.useState(false);
  const classes = useRowStyles();

  return (
    <React.Fragment>
      <TableRow className={classes.root}>
        <TableCell>
          <IconButton aria-label="expand row" size="small" onClick={() => setOpenSecondTable(!openSecondTable)}>
            {openSecondTable ? <KeyboardArrowDownIcon /> : <KeyboardArrowRight />}
          </IconButton>
        </TableCell>
        <TableCell align="center">{row.id}</TableCell>
        <TableCell align="center">{row.title}</TableCell>
        <TableCell align="center">{row.desc}</TableCell>
        <TableCell align="center">{row.type}</TableCell>
        <TableCell align="center">{row.hours}</TableCell>
        <TableCell align="center">{row.location}</TableCell>
        <TableCell align="center">{row.level}</TableCell>
        <TableCell align="center">{row.function}</TableCell>
        <TableCell align="center">{row.month}</TableCell>
        <TableCell align="center">{row.cost}</TableCell>
        <TableCell align="center">{row.bow}</TableCell>
        <TableCell align="center">
        <IconButton aria-label="delete" className={classes.margin}>
                <DeleteIcon fontSize="large" />
        </IconButton>
        </TableCell>
        <TableCell align="center">{row.status}</TableCell>
        <TableCell align="center">{row.job}</TableCell>
      </TableRow>
      <TableRow>
        <TableCell style={{ paddingBottom: 0, paddingTop: 0, paddingLeft: 0, paddingRight: 0 }} colSpan={15}>
          <Collapse in={openSecondTable} timeout="auto" unmountOnExit>
            
              <Typography component="h5" style={{ fontWeight: 'bold', textDecoration: 'underline',  marginLeft: "50px" }}>
                Indirect Reports
              </Typography>
              <TableContainer component={Paper}>
                <Table aria-label="collapsible table">
                    <TableHead>
                    <TableRow>
                        <TableCell style={{'width': '5%'}}/>
                        <TableCell align="center">Sr. No</TableCell>
                        <TableCell align="center">Job Title</TableCell>
                        <TableCell align="center">Description</TableCell>
                        <TableCell align="center">Job Type</TableCell>
                        <TableCell align="center">Hours</TableCell>
                        <TableCell align="center">Location</TableCell>
                        <TableCell align="center">Level</TableCell>
                        <TableCell align="center">Status</TableCell>
                    </TableRow>
                    </TableHead>
                    <TableBody>

                     {second.map((row) => (
                      <Secondrow key={row.id} row={row} />
                     ))}
                    </TableBody>
                </Table>
             </TableContainer>
            
          </Collapse>
        </TableCell>
      </TableRow>
    </React.Fragment>
  );
}

function Secondrow(props) {
  const { row } = props;
  const [openThirdTable, setOpenThirdTable] = React.useState(false);
  const classes = useRowStyles();

  return (
    <React.Fragment>
      <TableRow className={classes.root}>
        <TableCell>
          <IconButton aria-label="expand row" size="small" onClick={() => setOpenThirdTable(!openThirdTable)}>
            {openThirdTable ? <KeyboardArrowDownIcon /> : <KeyboardArrowRight />}
          </IconButton>
        </TableCell>
        <TableCell align="center">{row.id}</TableCell>
        <TableCell align="center">{row.title}</TableCell>
        <TableCell align="center">{row.desc}</TableCell>
        <TableCell align="center">{row.type}</TableCell>
        <TableCell align="center">{row.hours}</TableCell>
        <TableCell align="center">{row.location}</TableCell>
        <TableCell align="center">{row.level}</TableCell>
        <TableCell align="center">{row.status}</TableCell>
      </TableRow>
      <TableRow>
        <TableCell style={{ paddingBottom: 0, paddingTop: 0, paddingLeft: 0, paddingRight: 0 }} colSpan={15}>
          <Collapse in={openThirdTable} timeout="auto" unmountOnExit>
            
              <Typography component="h5" style={{ fontWeight: 'bold', textDecoration: 'underline',  marginLeft: "50px" }}>
                Indirect Reports
              </Typography>
              <TableContainer component={Paper}>
                <Table aria-label="collapsible table">
                    <TableHead>
                    <TableRow>
                        <TableCell style={{'width': '5%'}}/>
                        <TableCell align="center">Sr. No</TableCell>
                        <TableCell align="center">Job Title</TableCell>
                        <TableCell align="center">Description</TableCell>
                        <TableCell align="center">Job Type</TableCell>
                        <TableCell align="center">Hours</TableCell>
                        <TableCell align="center">Location</TableCell>
                        <TableCell align="center">Level</TableCell>
                        <TableCell align="center">Function</TableCell>
                        <TableCell align="center">Start Month</TableCell>
                        <TableCell align="center">Est. Cost</TableCell>
                        <TableCell align="center">Est. Bow Wave</TableCell>
                        <TableCell align="center">Action</TableCell>
                        <TableCell align="center">Status</TableCell>
                        <TableCell align="center">ATS Job Req</TableCell>
                    </TableRow>
                    </TableHead>
                    <TableBody>

                     {second.map((row) => (
                      <Firstrow key={row.id} row={row} />
                     ))}
                    </TableBody>
                </Table>
             </TableContainer>
            
          </Collapse>
        </TableCell>
      </TableRow>
    </React.Fragment>
  );
}

export default function CollapsibleTable() {
  return (
    <TableContainer component={Paper}>
        <Typography component="h5" style={{ fontWeight: 'bold', textAlign: "left", marginLeft: "50px" }}>
            People Manager Reports
        </Typography>
      <Table aria-label="collapsible table">
        <TableHead>
          <TableRow>
            <TableCell style={{'width': '5%'}}/>
            <TableCell  style={{fontWeight: 'bold', fontSize: '13px'}} align="center">Sr. No</TableCell>
            <TableCell  style={{fontWeight: 'bold', fontSize: '13px'}} align="center">Manager Name</TableCell>
            <TableCell  style={{fontWeight: 'bold', fontSize: '13px'}} align="center">Sub Cost Center</TableCell>
            <TableCell  style={{fontWeight: 'bold', fontSize: '13px'}} align="center">Current HC</TableCell>
            <TableCell  style={{fontWeight: 'bold', fontSize: '13px'}} align="center">Incremental HC</TableCell>
            <TableCell  style={{fontWeight: 'bold', fontSize: '13px'}} align="center">Budgeted</TableCell>
            <TableCell  style={{fontWeight: 'bold', fontSize: '13px'}} align="center">Remaining</TableCell>
            <TableCell  style={{fontWeight: 'bold', fontSize: '13px'}} align="center">Est. Bow Wave Impact (Next Year)</TableCell>
            <TableCell  style={{fontWeight: 'bold', fontSize: '13px'}} align="center">Status</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <Mainrow key={row.id} row={row} />
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
